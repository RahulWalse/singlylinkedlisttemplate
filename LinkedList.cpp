/**
 * @file LinkedList.cpp
 * @author Rahul Walse
 * @brief Source file for implementation of member functions and operations on singly linked list
 * @version 0.1
 * @date 2022-04-27
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <iostream>
#include "LinkedList.h"
#include "ListException.h"

/* Definition of default constructor */
template <typename T>
LinkedList<T>::LinkedList() : head(nullptr), tail(nullptr)
{
    /*
        Set the head and tail pointer to null.
    */
}

/* Defintion of destructor */
template <typename T>
LinkedList<T>::~LinkedList()
{
    Node<T>* target;    //- Node to be deleted
    /* Traverse the list */
    while(head != nullptr)
    {
        /* Special case: only one/last node in the list */
        if(head == tail)
        {
            target = head;          //- Set the target node to current head
            head = head->getNext(); //- Move head to next node
            tail = tail->getNext(); //- Move tail to next node
            delete target;          //- Release the memory by deleting the object
        }
        else
        {
            target = head;              //- Set the target node to current head
            head = head->getNext();     //- Move head to next node
            delete target;              //- Release the memory by deleting the object
        }
    }

}

/* Member function definition to add new node at the beginning of list */
template <typename T>
void LinkedList<T>::PushFront(const T& dataObj)
{
    Node<T>* newNode = new Node<T>(dataObj);    //- New node to be added to the list

    // Check if the list is empty
    if(IsEmpty())
        head = tail = newNode;  //- First node, set head, tail to new node
    else
    {
        newNode->setNext(head);     //- Set new node's next to current head
        head = newNode;             //- Set new node as head
    }
}

/* Implementation of function to add new node at the end of list */
template <typename T>
void LinkedList<T>::PushBack(const T& dataObj)
{
    Node<T>* newNode = new Node<T>(dataObj);    //- New node to added to the list

    if(IsEmpty())
        head = tail = newNode;
    else
    {
        tail->setNext(newNode);
        tail = tail->getNext();
    }
}

/* Implementation of function to insert new node at specified position */
template <typename T>
void LinkedList<T>::Insert(const T& dataObj, const unsigned& position)
{
    /* Edge cases */
    if(position <= 1)
        PushFront(dataObj);
    else if(position > getNodeCount())
        PushBack(dataObj);
    else
    {
        Node<T>* newNode = new Node<T>(dataObj);    //- New node to be inserted
        unsigned insertPosition = 1;
        Node<T>* targetNode = head;

        // Traverse list one position before target position
        while(insertPosition < position - 1)
        {
            insertPosition++;
            targetNode = targetNode->getNext();
        }

        newNode->setNext(targetNode->getNext());    //- Set new node's next to target's next
        targetNode->setNext(newNode);               //- Set target's next to new node
    }
}

/* Member function definition to remove node from the beginning of the list */
template <typename T>
T LinkedList<T>::PopFront()
{
    // Check if the list is empty
    if(IsEmpty())
        throw EmptyListException("ERROR: Cannot delete! List is empty.");
    else
    {
        Node<T>* temp;  //- Pointer to node to be deleted
        T deletedData;  //- To store object to be deleted

        /* Special case: single node in list */
        if(head == tail)
        {
            temp = head;                    //- Set temp to head
            deletedData = temp->getData();  //- Store object to be deleted
            head = tail = nullptr;          //- Set head and tail to null
            delete temp;                    //- Delete object to free memory
            return deletedData;             //- Return the object
        }

        /* Case where more than one nodes in the list */
        temp = head;                    //- Set temp to head
        head = head->getNext();         //- Move head to next node in the list
        deletedData = temp->getData();  //- Store object to be deleted
        delete temp;                    //- Delete object to free memory
        return deletedData;             //- Return the object
    }
}

/* Implementation of function to remove node from the end of list */
template <typename T>
T LinkedList<T>::PopBack()
{
    if(IsEmpty())
        throw EmptyListException("ERROR: List is empty!");
    else
    {
        Node<T>* targetNode = head;
        Node<T>* prevNode;
        T deletedData;
        // Traverse the list
        while(targetNode->getNext() != nullptr)
        {
            prevNode = targetNode;
            targetNode = targetNode->getNext();
        }

        /* Special case: only one node in list */
        if(targetNode == head)
        {
            deletedData = targetNode->getData();
            head = tail = nullptr;
            delete targetNode;

            return deletedData;
        }
        else
        {
            prevNode->setNext(nullptr);
            tail = prevNode;
            deletedData = targetNode->getData();
            delete targetNode;

            return deletedData;
        }
    }
}

/* Implementation of function to delete node at specified position from the list */
template <typename T>
T LinkedList<T>::Delete(const unsigned& position)
{
    if(IsEmpty())
        throw EmptyListException("ERROR: List is empty!");
    else
    {
        /* Invalid positions */
        if(position < 1 || position > getNodeCount())
            throw InvalidPositionException("ERROR: Invalid position!");
        else
        {
            /* Special cases */
            if(position == 1)
                return PopFront();
            else if(position == getNodeCount())
                return PopBack();
            else
            {
                // Two pointers to adjacent nodes
                Node<T>* targetNode = head;
                Node<T>* traverseNode;
                T deletedData;
                unsigned targetPosition = 1;
                //- Traverse list to target node position
                while (targetPosition < position)
                {
                    targetPosition++;
                    traverseNode = targetNode;
                    targetNode = targetNode->getNext();
                }
                // Link target node's previous to target node's next node; delete and unlink target node
                deletedData = targetNode->getData();
                traverseNode->setNext(targetNode->getNext());
                targetNode->setNext(nullptr);
                delete targetNode;

                return deletedData;
            }
        }
    }
}

/* Member function definition to check if the list is empty */
template <typename T>
bool LinkedList<T>::IsEmpty() const
{
    return head == nullptr;
}

/* Member function definition to print the list of objects */
template <typename T>
void LinkedList<T>::PrintList() const
{
    // Check if the list is empty
    if(IsEmpty())
        throw EmptyListException("ERROR: List is empty!");
    else
    {
        std::cout << "\nList of " << typeid(T).name() << ":\n";
        
        Node<T>* currNode = head;

        while(currNode != nullptr)
        {
            if(currNode == head)
                std::cout << currNode->getData();
            else
            {
                std::cout << "\n\t|\n\tV";
                std::cout << currNode->getData();
            }

            currNode = currNode->getNext();
        }
        std::cout << std::endl;
    }
}

/* Implementation of function to count the nodes in the list */
template <typename T>
unsigned LinkedList<T>::getNodeCount() const
{
    unsigned totalNodes = 0;

    Node<T>* currNode = head;

    while(currNode != nullptr)
    {
        ++totalNodes;
        currNode = currNode->getNext();
    }

    return totalNodes;
}