/**
 * @file LinkedList.h
 * @author Rahul Walse
 * @brief Class template for singly linked list containing Node of type T
 * @version 0.1
 * @date 2022-04-27
 * 
 * @copyright Copyright (c) 2022
 * 
 */

// Header guard
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "Node.h"

template <typename T>
class LinkedList
{
    private:
        // Data members
        Node<T>* head;
        Node<T>* tail;

        // Get count of nodes in list
        unsigned getNodeCount() const;

    public:
        // Special member functions
        LinkedList();
        ~LinkedList();

        // Operations on linkedlist

        // Adding new object to the list
        void PushFront(const T&);
        void PushBack(const T&);
        void Insert(const T&, const unsigned&);

        // Removing object from the list
        T PopFront();
        T PopBack();
        T Delete(const unsigned&);

        inline bool IsEmpty() const;
        void PrintList() const;
};

#endif  //!LINKEDLIST_H