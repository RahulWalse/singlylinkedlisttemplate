/**
 * @file ListException.h
 * @author Rahul Walse
 * @brief Header file to handle the exceptions for linked list
 * @version 0.1
 * @date 2022-04-27
 * 
 * @copyright Copyright (c) 2022
 * 
 */

// Header guard
#ifndef LISTEXCEPTION_H
#define LISTEXCEPTION_H

#include <exception>
#include <cstring>

class EmptyListException : public std::exception
{
    private:
        // Data members
        char message[50];

    public:
        // Parameterized constructor
        EmptyListException(const char* errorMessage)
        {
            strcpy(message, errorMessage);
        }

        const char* what() const override
        {
            return message;
        }
};

class InvalidPositionException : public std::exception
{
    private:
        // Data members
        char message[50];
    
    public:
        // Parameterized constructor
        InvalidPositionException(const char* errorMessage)
        {
            strcpy(message, errorMessage);
        }

        const char* what() const override
        {
            return message;
        }
};

#endif  //!LISTEXCEPTION_H