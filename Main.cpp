/**
 * @file Main.cpp
 * @author Rahul Walse
 * @brief Tester file to test and perform operations on singly linked list of type T
 * @version 0.1
 * @date 2022-04-27
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <iostream>
#include <string>
#include "Student.h"
#include "Node.cpp"
#include "LinkedList.h"
#include "LinkedList.cpp"
#include "ListException.h"

int main()
{
    // Linked list object to store student object
    LinkedList<Student> studentsList;

    // For storing student details
    std::string studentName;    double percentage;
    unsigned position = 0;

    // For menu options
    bool stay = true;   int option = 0;

    while(stay)
    {
        // Display menu options to the user
        std::cout << "\n1. Add new node at the beginning\n2. Add new node at the end\n3. Insert new node at specified position";
        std::cout << "\n4. Delete node from the front\n5. Delete node at the end\n6. Delete node at specified position";
        std::cout << "\n7. Print the list\n8. Exit\nChoose an option: ";
        std::cin >> option;

        switch (option)
        {
            case 1: /* Adding new node at the beginning */
                    std::cout << "\nEnter student details:\n";
                    std::cout << "Name: ";          std::cin >> studentName;
                    std::cout << "Percentage: ";    std::cin >> percentage;
                    // Add new node (Student) to the list
                    studentsList.PushFront(Student(studentName, percentage));
                    break;

            case 2: /* Adding new node at the end */
                    std::cout << "\nEnter student details:\n";
                    std::cout << "Name: ";          std::cin >> studentName;
                    std::cout << "Percentage: ";    std::cin >> percentage;
                    // Add new node (Student) to the list
                    studentsList.PushBack(Student(studentName, percentage));
                    break;

            case 3: /* Inserting new node at the specified position */
                    std::cout << "\nEnter student details:\n";
                    std::cout << "Name: ";          std::cin >> studentName;
                    std::cout << "Percentage: ";    std::cin >> percentage;
                    std::cout << "\nEnter position to be inserted: ";
                    std::cin >> position;
                    // Insert new node (Student) in the list
                    studentsList.Insert(Student(studentName, percentage), position);
                    break;

            case 4: /* Removing node at the beginning */
                    try
                    {
                        std::cout << "\nDeleted Node: " << studentsList.PopFront() << std::endl;
                    }
                    catch(EmptyListException error)
                    {
                        std::cerr << '\n' << error.what() << '\n';
                    }
                    break;

            case 5: /* Removing node at the end */
                    try
                    {
                        std::cout << "\nDeleted Node: " << studentsList.PopBack() << std::endl;
                    }
                    catch(EmptyListException error)
                    {
                        std::cerr << '\n' << error.what() << '\n';
                    }
                    break;

            case 6: /* Deleting node at specified position */
                    std::cout << "\nPosition to delete node at: ";
                    std::cin >> position;
                    try
                    {
                        std::cout << "\nDeleted Node: " << studentsList.Delete(position) << std::endl;
                    }
                    catch(EmptyListException error)
                    {
                        std::cerr << '\n' << error.what() << '\n';
                    }
                    catch(InvalidPositionException error)
                    {
                        std::cerr << '\n' << error.what() << '\n';
                    }
                    break;

            case 7: /* Printing the list of objects */
                    try
                    {
                        studentsList.PrintList();
                    }
                    catch(EmptyListException error)
                    {
                        std::cerr << '\n' << error.what() << '\n';
                    }
                    break;

            case 8: std::cout << "\nExiting the program...\n";
                    stay = false;
                    break;
            
            default:std::cout << "\nInvalid option! Please choose from options provided.\n";
                    break;
        }
    }
}