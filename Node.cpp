/**
 * @file Node.cpp
 * @author Rahul Walse
 * @brief Source file for node of type T in singly linked list defining the member functions
 * @version 0.1
 * @date 2022-04-27
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "Node.h"

/* Defintion of paramterized constructor */
template <typename T>
Node<T>::Node(const T& dataObj) : data(dataObj), next(nullptr)
{
    /*
        Initialize the data part with the data object and
        next pointer to null.
    */
}

/* Definition of accessor member function to get object */
template <typename T>
const T& Node<T>::getData() const
{
    return data;
}

/* Definition of accessor member function to get link to next node */
template <typename T>
Node<T>* Node<T>::getNext() const
{
    /*
        NOTE: Here the return type is of pointer to node of type T,
        hence the returning pointer should also be pointer to node
        of type T.
    */
    return next;
}

/* Definition of mutator member function to set link to next node */
template <typename T>
void Node<T>::setNext(Node* newLink)
{
    next = newLink;
}