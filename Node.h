/**
 * @file Node.h
 * @author Rahul Walse
 * @brief Class template for node in singly linked list to store T type of object
 * @version 0.1
 * @date 2022-04-27
 * 
 * @copyright Copyright (c) 2022
 * 
 */

// Header guard
#ifndef NODE_H
#define NODE_H

template <typename T>
class Node
{
    private:
        // Data members
        T data;
        Node* next;

    public:
        // Special member functions
        Node(const T&);

        // Accessor functions
        const T& getData() const;
        Node* getNext() const;

        // Mutator functions
        void setNext(Node*);
};

#endif  //!NODE_H