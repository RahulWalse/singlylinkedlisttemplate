/**
 * @file Student.cpp
 * @author Rahul Walse
 * @brief Source file for implementation of member and non-member functions of Student class
 * @version 0.1
 * @date 2022-04-27
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <iostream>
#include "Student.h"

// Initialization of static member
unsigned Student::number = 101;

// Definition of parameterized constructor
Student::Student(const std::string& inputName, const double& percentage)
: rollNumber(number++), name(inputName), percentage(percentage)
{
    /* Initialize the data members with user provided values */
}

/* Accessor functions */
// Member function to return roll number
unsigned Student::getRollNumber() const
{
    return rollNumber;
}

// Member function to return name
std::string Student::getName() const
{
    return name;
}

// Member function to return percentage
double Student::getPercentage() const
{
    return percentage;
}

/* Operator overloading */

// Implementation of operator overloading for assignment '=' operator
Student& Student::operator=(const Student& rhsObj)
{
    if(this == &rhsObj)
        return *this;

    this->rollNumber = rhsObj.rollNumber;
    this->name = rhsObj.name;
    this->percentage = rhsObj.percentage;

    return *this;
}

// Friend function for overloading '<<' operator
std::ostream& operator<<(std::ostream& os, const Student& studentObj)
{
    os << "\nRoll Number: " << studentObj.rollNumber;
    os << "\nName: " << studentObj.name;
    os << "\nPercentage: " << studentObj.percentage;

    return os;
}